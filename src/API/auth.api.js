const express = require("express");

const AuthController = require("../controllers/auth.controller");

const router = express.Router();

router.get("/login", (req, res) => {
  const result = AuthController.login();
  res.send(result);
});

module.exports = router;
